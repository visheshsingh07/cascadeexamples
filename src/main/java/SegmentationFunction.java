import cascading.flow.FlowProcess;
import cascading.operation.BaseOperation;
import cascading.operation.Function;
import cascading.operation.FunctionCall;
import cascading.tuple.Fields;
import cascading.tuple.Tuple;
import cascading.tuple.TupleEntry;

/**
 * Created by varunb on 23/8/15.
 */
public class SegmentationFunction extends BaseOperation implements Function {

    public SegmentationFunction(){
        //super(2, new Fields("name"));
        super(1, new Fields("name"));  //for replace
    }

    @Override
    public void operate(FlowProcess flowProcess, FunctionCall functionCall) {
        Tuple res = Tuple.size(1);

        System.out.println("*** Inside Operation ***" + res.size());
        TupleEntry args = functionCall.getArguments();
        //TupleEntryCollector collector = args.getTuple().;

        int namepos = args.getFields().getPos("name");

        System.out.println("result ::" + res.size());
        String name = args.getString("name");

        name =  name.replace('B', 'C');

        res.set(0,name);

        System.out.println("result = " + res.getString(0)  + "  size " + res.size() + " op: " + functionCall.getOutputCollector().toString());
        functionCall.getOutputCollector().add(res);

    }
}
