import cascading.flow.FlowProcess;
import cascading.operation.BaseOperation;
import cascading.operation.Function;
import cascading.operation.FunctionCall;
import cascading.tuple.Tuple;
import cascading.tuple.TupleEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.rmi.runtime.Log;


/**
 * Created by satyadev on 9/30/2015.
 */
public class ReplaceFunction extends BaseOperation implements Function {

    private static final Logger LOG = LoggerFactory.getLogger(ReplaceFunction.class);


/*    public ReplaceFunction(){
        super(1);
	 LOG.info("Inside Replace Function Constructor");  
  }
*/
    @Override
    public void operate(FlowProcess flowProcess, FunctionCall functionCall) {
        LOG.info("Inside Replace Function operate() method");

        Tuple currentTuple =  Tuple.size(1); // Get one record

        LOG.info("Tuple value is : " + currentTuple);
        LOG.info("Size of the tuple is: " + currentTuple.size());
        for(int i=0;i<currentTuple.size();i++)
            LOG.info("Tuple Value in position "+i+" is "+currentTuple.getString(i));

        TupleEntry argumentsInTheTuple = functionCall.getArguments();

        LOG.info("Size of the Tuple Entry Object is : "+argumentsInTheTuple.size());
        for(int i=0;i<argumentsInTheTuple.size();i++)
            LOG.info("Argument "+i+" is"+argumentsInTheTuple.getString(i));
    }
}

