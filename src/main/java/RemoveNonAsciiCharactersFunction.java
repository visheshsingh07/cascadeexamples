import cascading.flow.FlowProcess;
import cascading.operation.BaseOperation;
import cascading.operation.Function;
import cascading.operation.FunctionCall;
import cascading.tuple.Fields;
import cascading.tuple.Tuple;
import cascading.tuple.TupleEntry;

/**
 * Created by satyadev on 10/6/2015.
 */
    public class RemoveNonAsciiCharactersFunction extends BaseOperation implements Function {

    String columnName;

    public RemoveNonAsciiCharactersFunction(String colName) {
        super(1,new Fields(colName));
        columnName = colName;

    }

    @Override
    public void operate(FlowProcess flowProcess, FunctionCall functionCall) {
        TupleEntry arguments = functionCall.getArguments();
        Tuple inputTuple = arguments.getTuple();
        Tuple outputTuple = new Tuple(inputTuple);

        int columnPosition = arguments.getFields().getPos(columnName);
	System.out.println("Column position is : "+ columnPosition);
        String columnValue = arguments.getString(columnName);
	System.out.println("Column value is : "+ columnValue);
        outputTuple.set(columnPosition,columnValue.replaceAll("[^\\x00-\\x7F]", ""));

	System.out.println("Column value after replace  is : "+ columnValue.replaceAll("[^\\x00-\\x7F]", ""));
        functionCall.getOutputCollector().add(outputTuple);
    }
}

