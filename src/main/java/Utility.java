import java.util.ArrayList;

/**
 * Created by satyadev on 10/3/2015.
 */
public class Utility {

    public static String removeNonAsciiCharacters(String asciiString){
        return asciiString.replaceAll("[^\\\\u0000-\\\\uFFFF]", "");
    }

    public static String getFormattedMobileNumber(String mobileNumber){
	System.out.println("Mobile number inside getFormattedMobile() method is "+mobileNumber);
        ArrayList<String> listOfMobileNumbers = removePrefixFromMobileNumber(mobileNumber);
        StringBuilder sb = new StringBuilder();
        for (String mobNumber : listOfMobileNumbers) {
            sb.append(mobNumber);
            sb.append(":");
        }
        return  sb.toString();
    }

    static ArrayList<String> removePrefixFromMobileNumber(String mobileNumber){
	System.out.println("Mobile number inside removePrefix() method is "+mobileNumber);
        ArrayList<String> listOfMobileNumbers = new ArrayList<>();
        int mobileNumberLength = mobileNumber.length();
        while(mobileNumberLength >= 10) {
            if (mobileNumber.startsWith("+91")){
                mobileNumber = mobileNumber.substring(3);
		System.out.println("Mobile number after stripping +91 is "+mobileNumber);	
	    }
            else if (mobileNumber.startsWith("0")){

                mobileNumber = mobileNumber.substring(1);
	    	System.out.println("Mobile number after stripping 0 is "+mobileNumber);
	    }
           /* if(mobileNumberLength > 10){
                mobileNumber = mobileNumber.substring(10);
	    	System.out.println("Second Mobile number is  "+mobileNumber);
	    }
*/           
  if(mobileNumber.length() <= 10) {
		System.out.println("Mobile number added to the list is "+mobileNumber); 
               listOfMobileNumbers.add(mobileNumber);
                break;
            }
            listOfMobileNumbers.add(mobileNumber);
        }

        return listOfMobileNumbers;
    }

}

