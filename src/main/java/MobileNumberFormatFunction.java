import cascading.flow.FlowProcess;
import cascading.operation.BaseOperation;
import cascading.operation.Function;
import cascading.operation.FunctionCall;
import cascading.tuple.Fields;
import cascading.tuple.Tuple;
import cascading.tuple.TupleEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by satyadev on 10/3/2015.
 */
public class MobileNumberFormatFunction extends BaseOperation implements Function {

	private static final Logger LOG = LoggerFactory.getLogger(MobileNumberFormatFunction.class);

      public MobileNumberFormatFunction() {
        super(1, new Fields("mobile"));
     }


    @Override
    public void operate(FlowProcess flowProcess, FunctionCall functionCall) {
        TupleEntry arguments = functionCall.getArguments();
//        Tuple result = Tuple.size(3);
//	Tuple result = new Tuple();
//	System.out.println("Tuple value while initilialization: "+ result);
    	Tuple inputTuple = arguments.getTuple();
        Tuple result = new Tuple(inputTuple);
	System.out.println("Tuple value of Input Tuple:  "+ inputTuple);
	
	System.out.println("Size of input tuple is: "+ inputTuple.size());
	System.out.println("Tuple value while initilialization: "+ result);
 
	System.out.println("Size of output tuple while insitialization is: "+ result.size());
        
	int mobilePos = arguments.getFields().getPos("mobile");
        String mobileNumber = arguments.getString("mobile");
	System.out.println("Mobile Number is :"+mobileNumber);
        mobileNumber = Utility.getFormattedMobileNumber(mobileNumber);
        result.set(mobilePos,mobileNumber);
	//  arguments.setString("mobile",mobileNumber);
      
	System.out.println("Tuple value after setting the result is : "+ result);

	System.out.println("Size of result tuple is: "+ result.size());
        functionCall.getOutputCollector().add(result);



    }
}

