import cascading.flow.FlowProcess;
import cascading.operation.BaseOperation;
import cascading.operation.Filter;
import cascading.operation.FilterCall;
import cascading.tuple.Fields;
import cascading.tuple.TupleEntry;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by satyadev on 10/2/2015.
 */
public class EmailPatternFilter extends BaseOperation implements Filter {

//    static Fields fieldInputs = new Fields("name", "mobile", "email");
    Pattern EMAIL_PATTERN = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");

    @Override
    public boolean isRemove(FlowProcess flowProcess, FilterCall filterCall) {
        TupleEntry arguments = filterCall.getArguments();
        if(EMAIL_PATTERN.matcher(arguments.getString(0)).matches())
            return false;
        return true;
    }
}

