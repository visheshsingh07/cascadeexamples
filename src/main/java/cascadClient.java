import cascading.flow.Flow;
import cascading.flow.FlowDef;
import cascading.flow.hadoop2.Hadoop2MR1FlowConnector;
import cascading.operation.DebugLevel;
import cascading.operation.expression.ExpressionFilter;
import cascading.operation.expression.ExpressionFunction;
import cascading.pipe.Each;
import cascading.pipe.Pipe;
import cascading.property.AppProps;
import cascading.scheme.Scheme;
import cascading.scheme.hadoop.TextDelimited;
import cascading.tap.SinkMode;
import cascading.tap.Tap;
import cascading.tap.hadoop.Hfs;
import cascading.tuple.Fields;

import java.util.Properties;


/**
 * M/R
 * Program M,R As a client -> Yarn...
 *
 *
 * P1 -> P2 -> P3 -> HDFS
 *
 * Streaming?
 * subassemblies
 *
 * Tez
 */
public class cascadClient {

    public static void main(String[] args) {

        Scheme schIn1 = new TextDelimited( new Fields("id_1", "oddeven_1", "code"), ",");
        Scheme schIn2 = new TextDelimited(new Fields("id_2", "oddeven_2", "name"), ",");
        Scheme schOut1 = new TextDelimited( new Fields("id_2", "name"), true, ",");



        Tap srctap2 = new Hfs(schIn2, args[1]);//Database taps.
        //Tap srctap1 = new Hfs(schIn1 , "/user/hive/warehouse/pokernew/pokerPartition.csv");
                //"/user/hive/warehouse/pokernew/poker_1.csv");
                //"/user/hive/warehouse/pokernew/pokerPartition.csv");
        //"/user/hive/warehouse/pokernew/poker_1.csv");
        Tap sinkTap = new Hfs(new TextDelimited(true, ","), args[2], SinkMode.REPLACE);

        Pipe lhs = new Pipe("lhs");

        lhs = new Each(lhs, new Fields("oddeven_2"), new ExpressionFilter("oddeven_2 != 1", Integer.TYPE));

        ExpressionFunction ef = new ExpressionFunction(new Fields("segment"), " id_2 - 10", Integer.TYPE );

        SegmentationFunction sf = new SegmentationFunction();
        lhs = new Each(lhs, new Fields("name"), sf, Fields.REPLACE ); //replace
     /*
        lhs = new Each(lhs, new ExpressionFilter(" $0%10 != 0", Integer.TYPE));
        lhs = new Each(lhs, new Fields("name"), sf, Fields.REPLACE ); //replace
        lhs = new Discard(lhs, new Fields("name"));
       */

        Pipe rhs = new Pipe("rhs");

        //lhs = new Coerce(lhs, new Fields( "id_2","oddeven","name"), Integer.class, Integer.class, String.class);
        //lhs = new GroupBy(lhs,new Fields("oddeven"));
        //lhs = new Every(lhs, new FirstNBuffer(10), Fields.ALL);

        //lhs = new Every(lhs, new Fields("name","oddeven"), new MaxValue(new Fields("name")), Fields.ALL);
        //lhs = new Every(lhs, new FirstNBuffer(10), Fields.RESULTS ); // Use RESULTS for Buffer operations
        //lhs = new Discard(lhs, new Fields("name"));
        //lhs = new MinBy(lhs,new Fields("oddeven"), new Fields("id_2"), new Fields("MinId"));
        // Can be directly used, Grouping not required

        //lhs = new AverageBy(lhs,new Fields("oddeven"), new Fields("id_2"), new Fields("AvgId"));
        //lhs = new CountBy(lhs,new Fields("oddeven"), new Fields("count") );
        //lhs = new Discard(lhs, new Fields("name"));

        //Pipe join = new HashJoin(lhs, new Fields("id_1"), rhs, new Fields("id_2"), new InnerJoin());
        //join = new Each(join, new Fields("id_1", "name", "code", "id_2", "oddeven_1"), new Identity());

        FlowDef flowDef = FlowDef.flowDef().addSource(lhs, srctap2).addTailSink(lhs, sinkTap);
        //FlowDef flowDef = FlowDef.flowDef().addSource(lhs, srctap).addTailSink(lhs, sinkTap);
        flowDef.setDebugLevel( DebugLevel.VERBOSE );

        Properties properties = new Properties();
        AppProps.setApplicationJarClass(properties, cascadClient.class);
        Hadoop2MR1FlowConnector flowConnector = new Hadoop2MR1FlowConnector( properties );

        Flow flow = flowConnector.connect(flowDef);
        flow.writeDOT( "dot/Segment.dot" );
        flow.complete();

    }
}

