import cascading.flow.FlowProcess;
import cascading.operation.BaseOperation;
import cascading.operation.Function;
import cascading.operation.FunctionCall;
import cascading.tuple.Fields;
import cascading.tuple.Tuple;
import cascading.tuple.TupleEntry;

import java.util.regex.Pattern;

/**
 * Created by satyadev on 10/1/2015.
 */
public class EmailPatternMatchFunction extends BaseOperation implements Function {

   
	static  Fields outputFields = new Fields("name","mobile","email","Match/NoMatch");
        Pattern EMAIL_PATTERN = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");
   
//    public EmailPatternMatchFunction() {
//        super(1,outputFields);
//    }

    @Override
    public void operate(FlowProcess flowProcess, FunctionCall functionCall) {

        TupleEntry arguments = functionCall.getArguments();
	 Tuple inputTuple = arguments.getTuple();
	int matchPosition = arguments.getFields().getPos("Match/NoMatch");
        String matchOrNoMatch;
        Tuple outputTuple = new Tuple(inputTuple);
  
	if(EMAIL_PATTERN.matcher(arguments.getString(2)).matches()){
	  matchOrNoMatch = "Match";            
       }
        else
	{
		matchOrNoMatch = "No Match";	
	}
	   outputTuple.set(matchPosition,matchOrNoMatch);
        functionCall.getOutputCollector().add(outputTuple);


    }
}

