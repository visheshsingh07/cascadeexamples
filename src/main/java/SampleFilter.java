import cascading.flow.FlowProcess;
import cascading.operation.BaseOperation;
import cascading.operation.Filter;
import cascading.operation.FilterCall;
import cascading.tuple.Fields;
import cascading.tuple.TupleEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Created by satyadev on 10/1/2015.
 */
public class SampleFilter extends BaseOperation implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(SampleFilter.class);

    String filterWord;

    public SampleFilter(String word) {
        //super(3)
        filterWord = word;
    }

    @Override
    public boolean isRemove(FlowProcess flowProcess, FilterCall filterCall) {
	System.out.println("Inside isRemove() method");
        TupleEntry arguments = filterCall.getArguments();
        return arguments.getString(2).contains(filterWord);
    }
}

